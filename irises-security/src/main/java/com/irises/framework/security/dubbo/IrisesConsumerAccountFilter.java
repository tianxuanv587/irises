package com.irises.framework.security.dubbo;

import com.irises.framework.thread.ThreadLocalHolder;
import org.apache.dubbo.common.extension.Activate;
import org.apache.dubbo.rpc.*;

import java.util.HashMap;

/**
 * <p>
 * 描述：Dubbo消费端过滤器
 * </p>
 *
 * @author 田轩
 * @since 2021/4/10
 */
@SuppressWarnings("all")
@Activate
public class IrisesConsumerAccountFilter implements Filter {

    @Override
    public Result invoke(Invoker<?> invoker, Invocation invocation) throws RpcException {
        //从Security中获取当前登录的用户，如果有加入dubbo上下文中
        /*Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if(null != authentication) {
            Object principal = authentication.getPrincipal();
            if(null != principal && !"anonymousUser".equals(principal)) {
                Map map = Jackson.jsonParseObject(Jackson.toJSONString(principal), Map.class);
                String userId = String.valueOf(map.get("id"));
                RpcContext.getContext().setAttachments(new HashMap<String,String>(){{put("userId",userId);}});
            }
        }*/

        Long userId = ThreadLocalHolder.userThreadLocal.get();
        if(null != userId) {
            RpcContext.getContext().setAttachments(new HashMap<String,String>(){{put("userId",String.valueOf(userId));}});
        }
        return invoker.invoke(invocation);
    }
}
