package com.irises.framework.security.support;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 描述：
 * </p>
 *
 * @author 田轩
 * @since 2020/11/5
 */
public class ValidateCodeSupportWrapper {

    private RedisValidateCodeSupport redisValidateCodeSupport;

    private SessionValidateCodeSupport sessionValidateCodeSupport;

    public ValidateCodeSupportWrapper(RedisValidateCodeSupport redisValidateCodeSupport, SessionValidateCodeSupport sessionValidateCodeSupport) {
        this.redisValidateCodeSupport = redisValidateCodeSupport;
        this.sessionValidateCodeSupport = sessionValidateCodeSupport;
    }

    public String doGenerateCode(String mobile, HttpServletRequest request) {
        if(null != redisValidateCodeSupport) {
            return redisValidateCodeSupport.generateCode(mobile);
        } else if(null != sessionValidateCodeSupport){
            return sessionValidateCodeSupport.generateCode(mobile,request);
        }
        return null;
    }

    public void doValidate(String mobile, String codeInRequest, HttpServletRequest request){
        if(null != redisValidateCodeSupport) {
            redisValidateCodeSupport.validate(mobile, codeInRequest);
        } else if(null != sessionValidateCodeSupport){
            sessionValidateCodeSupport.validate(mobile,codeInRequest, request);
        }
    }


}
