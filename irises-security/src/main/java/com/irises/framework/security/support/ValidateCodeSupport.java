package com.irises.framework.security.support;

/**
 * <p>
 * 描述： 短信验证码操作接口
 * </p>
 *
 * @author 田轩
 * @since 2020/10/31
 */
public interface ValidateCodeSupport {

    /**
     * 生成短信验证码
     * @return
     */
    String generateCode(String mobile);

    /**
     * 获取短信登录验证码
     */
    void validate(String mobile, String codeInRequest);
}
