package com.irises.framework.security.userdetails;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * <p>
 * 受限资源
 * </p>
 *
 * @author 田轩
 * @since 2019-07-10
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class IrisesRule implements Serializable {

    private static final long serialVersionUID = 1L;
    @JsonSerialize(using= ToStringSerializer.class)
    protected Long id;

    /**
     * 权限名称/编码
     */
    protected String code;

    /**
     * 权限中文名称
     */
    protected String name;

    /**
     * 授权访问地址
     */
    protected String url;

    /**
     * 请求方式
     */
    protected String method;


}
