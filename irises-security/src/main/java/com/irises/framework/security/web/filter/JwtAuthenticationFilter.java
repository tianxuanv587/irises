package com.irises.framework.security.web.filter;

import com.irises.framework.security.constant.Constants;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author:
 * @date: 2019/4/8 15:28
 * @description:token 校验过滤器
 */
public class JwtAuthenticationFilter extends OncePerRequestFilter {

    private StringRedisTemplate redisTemplate;

    public JwtAuthenticationFilter(StringRedisTemplate redisTemplate){
        this.redisTemplate = redisTemplate;
    }
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        String token = request.getHeader(Constants.SPRING_SECURITY_HEADER_NAME);
        if (!StringUtils.hasText(token)) {
            chain.doFilter(request, response);
        } else {
            Claims claims = null;
            try{
                claims = Jwts.parser()
                        .setSigningKey(Constants.JWT_SECRET_CIPHER)
                        .parseClaimsJws(token).getBody();
            }catch (ExpiredJwtException e) {
                //如果令牌过期
                //不再校验客户端token是否过期,由服务器端进行过期
                /*String contextKey = IrisesSecurityUtils.getContextKey(request);
                redisTemplate.delete(contextKey);
                WebResponse r = new WebResponse();
                r.setCode(500);
                r.setMessage("令牌过期");
                r.setData(e.getMessage());
                response.setStatus(HttpServletResponse.SC_OK);
                response.setContentType(MediaType.APPLICATION_JSON_VALUE);
                response.setCharacterEncoding(StandardCharsets.UTF_8.toString());
                response.getWriter().write(JSONUtil.toJsonStr(r));
                return;*/
            } catch (Exception e) {
                //如果解析token报错，则Token乱写的
                chain.doFilter(request, response);
                return;
            }
            if(null != claims) {
                String userId = claims.get(Constants.SPRING_SECURITY_CONTEXT_USER_KEY,String.class);
                String tokenId = claims.get(Constants.SPRING_SECURITY_CONTEXT_TOKEN_ID,String.class);
                String username = claims.get(Constants.SPRING_SECURITY_USERNAME_KEY,String.class);
                String loginModel = claims.get(Constants.SPRING_SECURITY_LOGIN_MODE_KEY, String.class);

                request.setAttribute(Constants.SPRING_SECURITY_CONTEXT_USER_KEY,userId);
                request.setAttribute(Constants.SPRING_SECURITY_CONTEXT_TOKEN_ID,tokenId);
                request.setAttribute(Constants.SPRING_SECURITY_USERNAME_KEY,username);
                request.setAttribute(Constants.SPRING_SECURITY_LOGIN_MODE_KEY,loginModel);
            }
            chain.doFilter(request, response);
        }

    }
}
