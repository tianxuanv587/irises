package com.irises.framework.security.web.authentication;

import com.irises.framework.security.authentication.SmsCodeAuthenticationToken;
import com.irises.framework.security.constant.Constants;
import com.irises.framework.security.userdetails.SecurityUser;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.util.StringUtils;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.UUID;

public class IrisesSessionSmsCodeAuthenticationFilter extends AbstractAuthenticationProcessingFilter {
    private boolean postOnly = true;    //指定当前过滤器是否只处理POST请求

    public IrisesSessionSmsCodeAuthenticationFilter(AuthenticationManager authenticationManager, AuthenticationSuccessHandler successHandler, AuthenticationFailureHandler failureHandler) {
        super(new AntPathRequestMatcher("/login/mobile", "POST")); //指定当前过滤器处理的请求
        this.setAuthenticationSuccessHandler(successHandler);
        this.setAuthenticationFailureHandler(failureHandler);
        this.setAuthenticationManager(authenticationManager);
    }

    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
        if (request.getRequestURI().equals("/login/mobile") && this.postOnly && !request.getMethod().equals("POST")) {
            throw new AuthenticationServiceException("Authentication method not supported: " + request.getMethod());
        } else {
            String mobile = (String)request.getAttribute(Constants.SPRING_SECURITY_LOGIN_MOBILE_KEY);
            String mode = (String)request.getAttribute(Constants.SPRING_SECURITY_LOGIN_MODE_KEY);
            if(StringUtils.isEmpty(mobile))  throw new AuthenticationServiceException("登录错误");
            mobile = mobile.trim();

            request.setAttribute(Constants.SPRING_SECURITY_LOGIN_MOBILE_KEY, mobile);
            request.setAttribute(Constants.SPRING_SECURITY_LOGIN_MODE_KEY, mode);
            SmsCodeAuthenticationToken authRequest = new SmsCodeAuthenticationToken(mobile);
            this.setDetails(request, authRequest);
            return this.getAuthenticationManager().authenticate(authRequest);
        }
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) throws IOException, ServletException {
        SecurityUser user =(SecurityUser)authResult.getPrincipal();
        request.setAttribute(Constants.SPRING_SECURITY_CONTEXT_USER_KEY,user.getId());
        request.setAttribute(Constants.SPRING_SECURITY_CONTEXT_TOKEN_ID, UUID.randomUUID().toString());
        super.successfulAuthentication(request, response, chain, authResult);
    }

    /**
     * 把请求的详情，例如请求ip、SessionId等设置到验证请求中去
     * @param request
     * @param authRequest
     */
    protected void setDetails(HttpServletRequest request, SmsCodeAuthenticationToken authRequest) {
        authRequest.setDetails(this.authenticationDetailsSource.buildDetails(request));
    }

}