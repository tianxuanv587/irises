package com.irises.framework.security.dubbo;

import com.irises.framework.thread.ThreadLocalHolder;
import org.apache.dubbo.common.extension.Activate;
import org.apache.dubbo.rpc.*;
import org.springframework.util.StringUtils;

/**
 * <p>
 * 描述：Dubbo服务端过滤器
 * </p>
 *
 * @author 田轩
 * @since 2021/4/10
 */
@SuppressWarnings("all")
@Activate
public class IrisesProviderAccountFilter implements Filter {

    @Override
    public Result invoke(Invoker<?> invoker, Invocation invocation) throws RpcException {
        //从dubbo上下文中获取操作用户的ID,存储到ThreadLocal中
        String userId = RpcContext.getContext().getAttachment("userId");
        if(StringUtils.hasText(userId)) {
            ThreadLocalHolder.userThreadLocal.set(Long.valueOf(userId));
        }
        return invoker.invoke(invocation);
    }
}
