package com.irises.framework.security.access.intercept;

import com.irises.framework.security.constant.Constants;
import com.irises.framework.security.userdetails.IrisesRule;
import com.irises.framework.security.userdetails.IrisesSecurityService;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.access.SecurityConfig;
import org.springframework.security.web.FilterInvocation;
import org.springframework.security.web.access.intercept.FilterInvocationSecurityMetadataSource;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.Collection;
import java.util.List;


/**
 * 从加载受限制资源信息
 */
public class IrisesFilterInvocationSecurityMetadataSource implements FilterInvocationSecurityMetadataSource {

    private IrisesSecurityService irisesSecurityService;

    public IrisesFilterInvocationSecurityMetadataSource(IrisesSecurityService irisesSecurityService) {
        this.irisesSecurityService = irisesSecurityService;
    }

    /**
     * 校验客户端请求是否在权限管辖范围内，是则交由 CustomAccessDecisionManager  判定当前用户是否拥有此权限，否则允许访问(无禁止，即可为)
     * @param o  客户端请求对象
     * @return
     * @throws IllegalArgumentException
     */
    public Collection<ConfigAttribute> getAttributes(Object o) throws IllegalArgumentException {
        List<IrisesRule> rules = loadResourceDefine();
        //object 中包含用户请求的request 信息
        HttpServletRequest request = ((FilterInvocation) o).getHttpRequest();
        AntPathRequestMatcher matcher;
        String resourceUrl;
        String resourceMethod;
        for (IrisesRule rule : rules) {
            resourceUrl = rule.getUrl();
            if(StringUtils.isEmpty(resourceUrl)) continue;
            resourceMethod = rule.getMethod();
            matcher = new AntPathRequestMatcher(resourceUrl,resourceMethod.equals(Constants.SPRING_SECURITY_METHOD_ALL) ? request.getMethod() : resourceMethod);
            if(matcher.matches(request)) {
                return SecurityConfig.createList(rule.getCode(),resourceMethod);
            }
        }
        return null;
    }

    @Override
    public Collection<ConfigAttribute> getAllConfigAttributes() {
        return null;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return true;
    }


    private List<IrisesRule> loadResourceDefine(){
        return irisesSecurityService.getResources();
    }
}
