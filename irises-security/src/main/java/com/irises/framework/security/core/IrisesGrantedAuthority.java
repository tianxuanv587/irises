package com.irises.framework.security.core;

import org.springframework.security.core.GrantedAuthority;


public class IrisesGrantedAuthority implements GrantedAuthority {
    private final String role;
    private final String method;

    public IrisesGrantedAuthority(String role, String method) {
        this.role = role;
        this.method = method;
    }

    @Override
    public String getAuthority() {
        return this.role + ":" + this.method;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        } else {
            return obj instanceof IrisesGrantedAuthority ? this.role.equals(((IrisesGrantedAuthority)obj).role) && this.method.equals(((IrisesGrantedAuthority)obj).method) : false;
        }
    }

    public int hashCode() {
        return (this.role + this.method).hashCode();
    }

    public String toString() {
        return this.role + ":" + this.method;
    }
}
