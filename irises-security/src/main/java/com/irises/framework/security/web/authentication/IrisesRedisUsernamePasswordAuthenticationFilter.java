package com.irises.framework.security.web.authentication;

import cn.hutool.json.JSONUtil;
import com.irises.framework.security.constant.Constants;
import com.irises.framework.security.userdetails.SecurityUser;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.util.StreamUtils;
import org.springframework.util.StringUtils;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

public class IrisesRedisUsernamePasswordAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    private StringRedisTemplate redisTemplate;

    public IrisesRedisUsernamePasswordAuthenticationFilter(AuthenticationManager authenticationManager, AuthenticationSuccessHandler successHandler, AuthenticationFailureHandler failureHandler, StringRedisTemplate redisTemplate) {
        this.setAuthenticationSuccessHandler(successHandler);
        this.setAuthenticationFailureHandler(failureHandler);
        this.setAuthenticationManager(authenticationManager);
        this.redisTemplate = redisTemplate;
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
        String body;
        try {
            body = StreamUtils.copyToString(request.getInputStream(), Charset.forName("UTF-8"));
        } catch (Exception e) {
            throw new AuthenticationServiceException("登录错误");
        }
        String username = null, password = null, mode = null;
        if(StringUtils.hasText(body)) {
            Map<String,String> params = JSONUtil.parseObj(body).toBean(Map.class);
            username = params.get("username");
            password = params.get("password");
            mode = params.get("mode");
        }
        request.setAttribute(SPRING_SECURITY_FORM_USERNAME_KEY, username);
        request.setAttribute(SPRING_SECURITY_FORM_PASSWORD_KEY, password);
        request.setAttribute(Constants.SPRING_SECURITY_LOGIN_MODE_KEY, mode);
        return super.attemptAuthentication(request,response);
    }


    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) throws IOException, ServletException {
        SecurityUser user =(SecurityUser)authResult.getPrincipal();
        request.setAttribute(Constants.SPRING_SECURITY_CONTEXT_USER_KEY,user.getId());
        request.setAttribute(Constants.SPRING_SECURITY_CONTEXT_TOKEN_ID,UUID.randomUUID().toString());
        if(null != redisTemplate){
            Set<String> keys = redisTemplate.keys(Constants.SPRING_SECURITY_CONTEXT_KEY + ":" + user.getId() + ":*");
            if(null != keys && keys.size() >= Constants.MAX_LOGIN_NUMBER){
                // 如果当前登录账号数量已经到达上限,则踢出一个已登录的凭证
                redisTemplate.delete(keys.iterator().next());
//                throw new AuthenticationServiceException("当前账号已登录数量已达上限！");
            }
        }
        super.successfulAuthentication(request, response, chain, authResult);
    }

    @Override
    protected String obtainPassword(HttpServletRequest request) {
        return (String)request.getAttribute(SPRING_SECURITY_FORM_PASSWORD_KEY);
    }

    @Override
    protected String obtainUsername(HttpServletRequest request) {
        return (String)request.getAttribute(SPRING_SECURITY_FORM_USERNAME_KEY);
    }
}
