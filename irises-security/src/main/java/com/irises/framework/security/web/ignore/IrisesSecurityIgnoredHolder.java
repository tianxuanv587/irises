package com.irises.framework.security.web.ignore;

import org.springframework.context.ApplicationContext;
import org.springframework.security.web.DefaultSecurityFilterChain;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * <p>
 * 描述：忽略的权限
 * </p>
 *
 * @author 田轩
 * @since 2020/10/29
 */

public class IrisesSecurityIgnoredHolder {
    private ApplicationContext applicationContext;
    // 忽略的权限校验
    private String[] ignored;
    //已经默认授权的接口
    private String[] permitted;

    public IrisesSecurityIgnoredHolder(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    public void setIgnored(String[] ignored) {
        this.ignored =  null != ignored ? ignored : new String[]{};
    }


    public void setPermitted(String[] permitted) {
        this.permitted =  null != permitted ? permitted : new String[]{};
    }

    public List<String>  getPermittedAndIgnored(){
        List<String> urls =  new ArrayList<>();
        if(null != ignored && ignored.length > 0) {
            urls.addAll(Arrays.asList(ignored));
        }
        if(null != permitted && permitted.length > 0) {
            urls.addAll(Arrays.asList(permitted));
        }
        return urls;
    }

    /**
     * 强行介入spring security 添加忽略的请求配置，不建议使用
     * @param urls
     */
    public void addIgnore(String... urls) {
        FilterChainProxy obj = (FilterChainProxy) applicationContext.getBean("springSecurityFilterChain");
        List<SecurityFilterChain> securityFilterChains = (List<SecurityFilterChain>) getProperty(obj,"filterChains");
        for (String url : urls) {
            securityFilterChains.add(new DefaultSecurityFilterChain(new AntPathRequestMatcher(url)));
        }

    }

    private static Object getProperty(Object obj, String fieldName) {
        try {
            Field field = obj.getClass().getDeclaredField(fieldName);
            field.setAccessible(true);
            return field.get(obj);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
