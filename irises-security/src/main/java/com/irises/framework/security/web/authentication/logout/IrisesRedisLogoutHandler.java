package com.irises.framework.security.web.authentication.logout;


import com.irises.framework.security.constant.Constants;
import com.irises.framework.security.support.IrisesSecurityUtils;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.util.Assert;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class IrisesRedisLogoutHandler extends SecurityContextLogoutHandler {

    private StringRedisTemplate redisTemplate;

    public IrisesRedisLogoutHandler(StringRedisTemplate redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    @Override
    public void logout(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) {
        SecurityContext context = SecurityContextHolder.getContext();

        String userId = String.valueOf(httpServletRequest.getAttribute(Constants.SPRING_SECURITY_CONTEXT_USER_KEY));
        String tokenId = (String)httpServletRequest.getAttribute(Constants.SPRING_SECURITY_CONTEXT_TOKEN_ID);
        String username = String.valueOf(httpServletRequest.getAttribute(Constants.SPRING_SECURITY_USERNAME_KEY));
        String contextKey = IrisesSecurityUtils.getContextKey(httpServletRequest);
        Assert.notNull(userId, "userId required");
        Assert.notNull(tokenId, "token required");
        redisTemplate.delete(contextKey);
        context.setAuthentication((Authentication)null);
        //从ThreadLocal中清除Context
        SecurityContextHolder.clearContext();
    }

}
