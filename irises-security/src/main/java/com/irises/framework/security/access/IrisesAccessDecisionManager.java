package com.irises.framework.security.access;

import com.irises.framework.security.web.ignore.IrisesSecurityIgnoredHolder;
import org.springframework.security.access.AccessDecisionManager;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.FilterInvocation;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.servlet.http.HttpServletRequest;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class IrisesAccessDecisionManager implements AccessDecisionManager {

    private IrisesSecurityIgnoredHolder irisesSecurityIgnoredHandler;

    public IrisesAccessDecisionManager(IrisesSecurityIgnoredHolder irisesSecurityIgnoredHandler) {
        super();
        this.irisesSecurityIgnoredHandler = irisesSecurityIgnoredHandler;
    }

    /**
     *   判定当前登录用户是否拥有 访问限定资源的权限
     * @param authentication  是CustomUserService中循环添加到 GrantedAuthority 对象中的权限信息集合.
     * @param o  包含客户端发起的请求的requset信息，可转换为 HttpServletRequest request = ((FilterInvocation) object).getHttpRequest();
     * @param configAttributes  为InvocationSecurityMetadataSource的getAttributes(Object object)这个方法返回的结果，
     * @throws AccessDeniedException
     * @throws InsufficientAuthenticationException
     */
    @Override
    public void decide(Authentication authentication, Object o, Collection<ConfigAttribute> configAttributes) throws AccessDeniedException, InsufficientAuthenticationException {
        //如果访问该接口不需要权限则结束验证
        if(null== configAttributes || configAttributes.size() <=0) {
            return;
        }
        //获取已认证的权限信息集合
        String principal = authentication.getPrincipal().toString(); //anonymousUser
        if("anonymousUser".equals(principal)) {  //如果是游客，则验证不通过
            throw new InsufficientAuthenticationException("游客不能访问该接口");
        }
        // 如果访问的接口需要权限,但是配置了跳过
        HttpServletRequest request = ((FilterInvocation) o).getHttpRequest();
        AntPathRequestMatcher matcher;

        List<String> permittedAndIgnored = irisesSecurityIgnoredHandler.getPermittedAndIgnored();
        for(String url : permittedAndIgnored) {
            matcher = new AntPathRequestMatcher(url,request.getMethod());
            if(matcher.matches(request)) {
                return;
            }
        }

        //访问当前资源需要的权限
        String needRole = configAttributes.stream().map(ConfigAttribute::getAttribute).collect(Collectors.joining(":"));
        //校验当前用户是否拥有该权限
        for(GrantedAuthority ga : authentication.getAuthorities()) {
            if(needRole.trim().equals(ga.getAuthority())) {
                return;
            }
        }
        throw new AccessDeniedException("权限不足");
    }

    @Override
    public boolean supports(ConfigAttribute configAttribute) {
        return true;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return true;
    }
}
