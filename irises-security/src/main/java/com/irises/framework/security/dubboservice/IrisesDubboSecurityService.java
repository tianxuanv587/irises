package com.irises.framework.security.dubboservice;

import com.irises.framework.security.userdetails.IrisesRule;
import com.irises.framework.security.userdetails.IrisesUser;

import java.util.List;

/**
 * <p>
 * 此接口为springSecurity返回用户账号、及用户权限
 *  此接口请勿作为业务接口调用
 * </p>
 *
 * @author 田轩
 * @since 2020/10/16
 */
public interface IrisesDubboSecurityService<R extends IrisesRule, U extends IrisesUser> {

    List<R> getResources();

    List<R> getResourcesByUserId(Long userId);

    U getIrisesUserDetails(String identifier);
}
