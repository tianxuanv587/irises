package com.irises.framework.security.web.filter;


import cn.hutool.json.JSONUtil;
import com.irises.framework.security.constant.Constants;
import com.irises.framework.security.exception.ValidateCodeException;
import com.irises.framework.security.support.ValidateCodeSupportWrapper;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.util.StreamUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Map;

/**
 *  短信验证码过滤器
 */
public class IrisesValidateCodeFilter extends OncePerRequestFilter {
    //认证失败处理器
    private AuthenticationFailureHandler authenticationFailureHandler;

    private ValidateCodeSupportWrapper validateCodeSupportWrapper;

    public IrisesValidateCodeFilter(AuthenticationFailureHandler authenticationFailureHandler, ValidateCodeSupportWrapper validateCodeSupportWrapper) {
        this.authenticationFailureHandler = authenticationFailureHandler;
        this.validateCodeSupportWrapper = validateCodeSupportWrapper;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
        if (httpServletRequest.getRequestURI().equals("/login/mobile") && httpServletRequest.getMethod().equalsIgnoreCase("post")) {
            try {
                //此处进行手机验证码验证
                String body;
                try {
                    body = StreamUtils.copyToString(httpServletRequest.getInputStream(), Charset.forName("UTF-8"));
                } catch (Exception e) {
                    throw new AuthenticationServiceException("登录错误");
                }
                String codeInRequest = "", mobile= "";
                if(StringUtils.hasText(body)) {
                    Map<String,String> params = JSONUtil.parseObj(body).toBean(Map.class);
                    codeInRequest = params.get(Constants.SPRING_SECURITY_LOGIN_CODE_KEY);
                    mobile = params.get(Constants.SPRING_SECURITY_LOGIN_MOBILE_KEY);
                    httpServletRequest.setAttribute(Constants.SPRING_SECURITY_LOGIN_MOBILE_KEY, mobile);
                    httpServletRequest.setAttribute(Constants.SPRING_SECURITY_LOGIN_MODE_KEY, params.get(Constants.SPRING_SECURITY_LOGIN_CODE_KEY));
                }
                mobile = mobile.trim();
                codeInRequest = codeInRequest.trim();
                validateCodeSupportWrapper.doValidate(mobile, codeInRequest, httpServletRequest);
            }catch (ValidateCodeException e){
                // 验证失败
                authenticationFailureHandler.onAuthenticationFailure(httpServletRequest,httpServletResponse,e);
                return;
            }
        }
        //如果不是登录请求，直接调用后面的过滤器链
        filterChain.doFilter(httpServletRequest,httpServletResponse);
    }
}