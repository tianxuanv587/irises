package com.irises.framework.security.userdetails;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * <p>
 * 用户
 * </p>
 *
 * @author 田轩
 * @since 2019-07-10
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class IrisesUser implements Serializable {

    private static final long serialVersionUID = 1L;

    @JsonSerialize(using= ToStringSerializer.class)
    protected Long id;

    /**
     * 用户名
     */
    protected String identifier;

    /**
     * 密码
     */
    protected String credential;

    /**
     * 是否启用
     */
    protected boolean enabled;
}
