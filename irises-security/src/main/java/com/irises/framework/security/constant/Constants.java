package com.irises.framework.security.constant;

import cn.hutool.core.io.resource.ClassPathResource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;

import java.util.Properties;

@Slf4j
public class Constants {

    private Constants(){}
    /**
     * 用户SecurityContext的标识
     */
    public static final String SPRING_SECURITY_CONTEXT_KEY ;

    public static final String APP_NAME;

    public static final String SPRING_SECURITY_CONTEXT_USER_KEY = "USER_ID";
    public static final String SPRING_SECURITY_CONTEXT_TOKEN_ID = "TOKEN_ID";
    public static final String SPRING_SECURITY_METHOD_ALL = "ALL";
    public static final  long SPRING_SECURITY_KEY_EXPIRES_SHORT_TIME ;
    public static final  long SPRING_SECURITY_KEY_EXPIRES_LONG_TIME ;
    public static final String SPRING_SECURITY_LOGIN_MODE_SHORT = "short";
    public static final String SPRING_SECURITY_LOGIN_MODE_LONG = "long";
    /**
     * Headers中token的名称
     */
    public static final String SPRING_SECURITY_HEADER_NAME = "Authorization";
    public static final String SPRING_SECURITY_USERNAME_KEY = "username";
    /**
     * JWT 签名秘钥
     */
    public static final String JWT_SECRET_CIPHER ;

    /**
     * JWT客户段Token失效时间(Token采用服务器过期机制)
     */
    public static final int JWT_CLIENT_EXPIRE_TIME = 60 *  24 * 90;

    /**
     *  同时登陆最大数量
     */
    public static final int  MAX_LOGIN_NUMBER;

    /**
     * 手机登录手机号字段key
     */
    public static final String SPRING_SECURITY_LOGIN_MOBILE_KEY = "mobile";
    public static final String SPRING_SECURITY_LOGIN_CODE_KEY = "smsCode";
    public static final String SPRING_SECURITY_LOGIN_MODE_KEY = "mode";
    /**
     * 手机验证码redis前缀    irises:validate-code:手机号:验证码
     */
    public static final String STRING_SECURITY_SMS_CODE_KEY_PREFIX;

    static {
        Properties config = new Properties();
        try {
            ClassPathResource resource = new ClassPathResource("security.properties");
            config.load(resource.getStream());
        } catch (Exception e) {
            log.info("读取Security配置(security.properties)失败，将使用默认配置！");
        }
        String appName = config.getProperty("security.app.name");
        String jwtCipher = config.getProperty("security.jwt.cipher");
        String contextKeyDefaultExpires = config.getProperty("security.context-key.default-expires");
        String maxLoginNumber = config.getProperty("security.max-login-number");
        APP_NAME = StringUtils.hasText(appName) ? appName.trim() : "irises";
        SPRING_SECURITY_KEY_EXPIRES_SHORT_TIME = StringUtils.hasText(contextKeyDefaultExpires) ? Long.parseLong(contextKeyDefaultExpires) : 60 *  2;
        SPRING_SECURITY_KEY_EXPIRES_LONG_TIME = StringUtils.hasText(contextKeyDefaultExpires) ? Long.parseLong(contextKeyDefaultExpires) : 60 *  24 * 90;
        SPRING_SECURITY_CONTEXT_KEY = APP_NAME + ":SECURITY_CONTEXT";
        JWT_SECRET_CIPHER = StringUtils.hasText(jwtCipher) ? jwtCipher : "zhaNvGuoYuQing";
        //默认只能登录一个
        MAX_LOGIN_NUMBER = StringUtils.hasText(maxLoginNumber) ? Integer.parseInt(maxLoginNumber) : 1;
        STRING_SECURITY_SMS_CODE_KEY_PREFIX = APP_NAME + ":validate-code:";
    }
}
