package com.irises.framework.security.support;

import cn.hutool.core.util.RandomUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.irises.framework.security.constant.Constants;
import com.irises.framework.security.exception.ValidateCodeException;
import com.irises.framework.security.model.IrisesValidateCode;
import com.irises.framework.tools.json.Jackson;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * <p>
 * 描述： 使用redis存储验证的实现
 * </p>
 *
 * @author 田轩
 * @since 2020/10/31
 */
public class SessionValidateCodeSupport {

    public String generateCode(String mobile, HttpServletRequest request) {
        if(StringUtils.isEmpty(mobile)) throw new ValidateCodeException("手机号不能为空！");
        StringBuffer code = new StringBuffer();
        for(int i = 0; i < 6; i++) {
            code.append(RandomUtil.randomInt(0, 9));
        }
        HttpSession session = request.getSession();
        //将验证码存进Session，默认5分钟过期
//        session.setAttribute(Constants.STRING_SECURITY_SMS_CODE_KEY_PREFIX + mobile, new IrisesValidateCode(code.toString(), 60 * 5));
        IrisesValidateCode validateCode = new IrisesValidateCode(code.toString(), 60 * 5);
        session.setAttribute(Constants.STRING_SECURITY_SMS_CODE_KEY_PREFIX + mobile, Jackson.toJSONString(validateCode));
        return code.toString();
    }

    public void validate(String mobile, String codeInRequest, HttpServletRequest request) {
        String key = Constants.STRING_SECURITY_SMS_CODE_KEY_PREFIX + mobile;
        HttpSession session = request.getSession();
        String validateCodeStr = (String)session.getAttribute(key);

        if(null == validateCodeStr){
            throw new ValidateCodeException("验证码已过期！");
        }

        JSONObject jsonObject = JSONUtil.parseObj(validateCodeStr);
        IrisesValidateCode validateCode = null != jsonObject ? jsonObject.toBean(IrisesValidateCode.class) : null;

        if(validateCode.isExpried()){
            session.removeAttribute(key);
            throw new ValidateCodeException("验证码已过期！");
        }
        if(!validateCode.getCode().equals(codeInRequest)) {
            throw new ValidateCodeException("验证码不正确！");
        }
        //从Redis中删除验证码
        session.removeAttribute(key);
    }
    /*public HttpSession obtainSession(){
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
        return request.getSession();
    }*/
}
