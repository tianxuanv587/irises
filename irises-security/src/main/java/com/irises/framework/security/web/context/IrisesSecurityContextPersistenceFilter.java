package com.irises.framework.security.web.context;

import org.springframework.security.web.context.SecurityContextPersistenceFilter;

public class IrisesSecurityContextPersistenceFilter extends SecurityContextPersistenceFilter {
    public IrisesSecurityContextPersistenceFilter(RedisSecurityContextRepository redisSecurityContextRepository) {
        super(redisSecurityContextRepository);
    }
}
