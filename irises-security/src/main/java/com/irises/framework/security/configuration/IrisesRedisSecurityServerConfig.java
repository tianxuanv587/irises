package com.irises.framework.security.configuration;


import com.irises.framework.security.access.IrisesAccessDecisionManager;
import com.irises.framework.security.access.intercept.IrisesFilterInvocationSecurityMetadataSource;
import com.irises.framework.security.access.intercept.IrisesSecurityInterceptor;
import com.irises.framework.security.dubboservice.IrisesDubboSecurityService;
import com.irises.framework.security.provider.IrisesSmsCodeAuthenticationProvider;
import com.irises.framework.security.support.RedisValidateCodeSupport;
import com.irises.framework.security.support.ValidateCodeSupportWrapper;
import com.irises.framework.security.userdetails.IrisesSecurityService;
import com.irises.framework.security.userdetails.IrisesSecurityServiceImpl;
import com.irises.framework.security.userdetails.IrisesUserDetailsService;
import com.irises.framework.security.web.IrisesAuthenticationEntryPoint;
import com.irises.framework.security.web.access.IrisesAccessDeniedHandler;
import com.irises.framework.security.web.authentication.IrisesAuthenticationFailureHandler;
import com.irises.framework.security.web.authentication.IrisesRedisAuthenticationSuccessHandler;
import com.irises.framework.security.web.authentication.IrisesRedisSmsCodeAuthenticationFilter;
import com.irises.framework.security.web.authentication.IrisesRedisUsernamePasswordAuthenticationFilter;
import com.irises.framework.security.web.authentication.logout.IrisesLogoutSuccessHandler;
import com.irises.framework.security.web.authentication.logout.IrisesRedisLogoutHandler;
import com.irises.framework.security.web.context.IrisesSecurityContextPersistenceFilter;
import com.irises.framework.security.web.context.RedisSecurityContextRepository;
import com.irises.framework.security.web.filter.IrisesValidateCodeFilter;
import com.irises.framework.security.web.filter.JwtAuthenticationFilter;
import com.irises.framework.security.web.filter.OptionsRequestFilter;
import com.irises.framework.security.web.ignore.IrisesSecurityIgnoredHolder;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.BeanIds;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.access.intercept.FilterSecurityInterceptor;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.context.SecurityContextPersistenceFilter;
import org.springframework.security.web.header.Header;
import org.springframework.security.web.header.writers.StaticHeadersWriter;
import org.springframework.web.filter.CorsFilter;

import java.util.Arrays;
import java.util.List;

@SuppressWarnings("all")
@EnableWebSecurity
public class IrisesRedisSecurityServerConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private StringRedisTemplate redisTemplate;
    @Autowired
    private ApplicationContext applicationContext;
    @DubboReference
    private IrisesDubboSecurityService irisesDubboSecurityService;


    public IrisesFilterInvocationSecurityMetadataSource irisesFilterInvocationSecurityMetadataSource() {
        IrisesSecurityService irisesSecurityService = applicationContext.getBean(IrisesSecurityService.class);
        return new IrisesFilterInvocationSecurityMetadataSource(irisesSecurityService);
    }


    public IrisesSecurityInterceptor irisesSecurityInterceptor() {
        IrisesSecurityIgnoredHolder irisesSecurityIgnoredHandler = applicationContext.getBean(IrisesSecurityIgnoredHolder.class);
        IrisesSecurityInterceptor irisesSecurityInterceptor = new IrisesSecurityInterceptor(irisesFilterInvocationSecurityMetadataSource());
        irisesSecurityInterceptor.setMyAccessDecisionManager(new IrisesAccessDecisionManager(irisesSecurityIgnoredHandler));
        return irisesSecurityInterceptor;
    }

    @Bean
    @Scope("singleton")
    public IrisesSecurityService irisesSecurityService() {
        return new IrisesSecurityServiceImpl(irisesDubboSecurityService);
    }

    @Bean
    public IrisesUserDetailsService irisesUserDetailsService() {
        IrisesSecurityService irisesSecurityService = applicationContext.getBean(IrisesSecurityService.class);
        return new IrisesUserDetailsService(irisesSecurityService);
    }

    @Bean
    @Scope("singleton")
    public IrisesSecurityIgnoredHolder irisesSecurityIgnoredHandler() {
        return new IrisesSecurityIgnoredHolder(applicationContext);
    }

    @Bean(name = BeanIds.AUTHENTICATION_MANAGER)
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    public IrisesRedisUsernamePasswordAuthenticationFilter irisesUsernamePasswordAuthenticationFilter(AuthenticationManager authenticationManager) {
        return new IrisesRedisUsernamePasswordAuthenticationFilter(authenticationManager, new IrisesRedisAuthenticationSuccessHandler(redisTemplate), new IrisesAuthenticationFailureHandler(), redisTemplate);
    }

    @Bean
    public RedisSecurityContextRepository redisSecurityContextRepository() {
        return new RedisSecurityContextRepository(redisTemplate);
    }

    @Override
    public void configure(WebSecurity webSecurity) throws Exception {
        String[] ignored = this.ignored();
        IrisesSecurityIgnoredHolder irisesSecurityIgnoredHandler = applicationContext.getBean(IrisesSecurityIgnoredHolder.class);
        irisesSecurityIgnoredHandler.setIgnored(ignored);
        if (null != ignored && ignored.length > 0) {
            webSecurity.ignoring().antMatchers(ignored);
        }
    }

    /**
     * 手机登录过滤器
     *
     * @param authenticationManager
     * @return
     */
    @Bean
    public IrisesRedisSmsCodeAuthenticationFilter smsCodeAuthenticationFilter(AuthenticationManager authenticationManager) {
        return new IrisesRedisSmsCodeAuthenticationFilter(authenticationManager, new IrisesRedisAuthenticationSuccessHandler(redisTemplate), new IrisesAuthenticationFailureHandler(), redisTemplate);
    }

    @Bean
    public ValidateCodeSupportWrapper validateCodeSupportWrapper() {
        return new ValidateCodeSupportWrapper(new RedisValidateCodeSupport(redisTemplate), null);
    }

    /**
     * 登录地址为 "/login"，登录成功返回响应状态码
     * 退出登录的地址为 "/logout"，退出成功返回响应状态码
     * 禁用 CSRF
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        String[] permitted = this.permitted();
        IrisesSecurityIgnoredHolder irisesSecurityIgnoredHandler = applicationContext.getBean(IrisesSecurityIgnoredHolder.class);
        irisesSecurityIgnoredHandler.setPermitted(permitted);
        ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry expressionInterceptUrlRegistry = http.authorizeRequests();
        if (null != permitted && permitted.length > 0) {
            expressionInterceptUrlRegistry.antMatchers(permitted).permitAll();
        }
        http
//              .anyRequest().authenticated()  //任何请求都需要身份认证
                .logout().logoutUrl("/logout")
                .addLogoutHandler(new IrisesRedisLogoutHandler(redisTemplate))
                .logoutSuccessHandler(new IrisesLogoutSuccessHandler())
                .clearAuthentication(true).permitAll()
                .and()
                .exceptionHandling().authenticationEntryPoint(new IrisesAuthenticationEntryPoint())
                .accessDeniedHandler(new IrisesAccessDeniedHandler())
                .and()
                .csrf().disable()
                .formLogin().disable()
                .httpBasic().disable()
                .cors()
                .and()
                .headers().addHeaderWriter(new StaticHeadersWriter(headerWrite()));

        http.addFilterAfter(new OptionsRequestFilter(), CorsFilter.class);
        //替换系统默认的SecurityContextPersistenceFilter
        RedisSecurityContextRepository redisSecurityContextRepository = applicationContext.getBean(RedisSecurityContextRepository.class);
        http.addFilterAt(new IrisesSecurityContextPersistenceFilter(redisSecurityContextRepository), SecurityContextPersistenceFilter.class);
        //替换默认的权限判断拦截器
        http.addFilterAt(irisesSecurityInterceptor(), FilterSecurityInterceptor.class);
        //增加JWT过滤器，解析token
        http.addFilterBefore(new JwtAuthenticationFilter(redisTemplate), SecurityContextPersistenceFilter.class);
        //增加手机号登录 的过滤器
        //短信验证码过滤器，放在加载用户名密码过滤器的前面
        ValidateCodeSupportWrapper validateCodeSupportWrapper = applicationContext.getBean(ValidateCodeSupportWrapper.class);
        http.addFilterBefore(new IrisesValidateCodeFilter(new IrisesAuthenticationFailureHandler(), validateCodeSupportWrapper), UsernamePasswordAuthenticationFilter.class);
        IrisesUserDetailsService userDetailsService = applicationContext.getBean(IrisesUserDetailsService.class);
        IrisesRedisSmsCodeAuthenticationFilter smsCodeAuthenticationFilter = applicationContext.getBean(IrisesRedisSmsCodeAuthenticationFilter.class);
        IrisesSmsCodeAuthenticationProvider smsCodeAuthenticationProvider = new IrisesSmsCodeAuthenticationProvider(userDetailsService);
        http.authenticationProvider(smsCodeAuthenticationProvider).addFilterAfter(smsCodeAuthenticationFilter, UsernamePasswordAuthenticationFilter.class);
    }

    /**
     * 配置自定义userDetails从数据库获取用户信息
     *
     * @param builder
     * @throws Exception
     */
    @Override
    public void configure(AuthenticationManagerBuilder builder) throws Exception {
        IrisesUserDetailsService userDetailsService = applicationContext.getBean(IrisesUserDetailsService.class);
        builder.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
    }

    /**
     * 密码加密
     */
    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    /**
     * 此数组中定义的路径，只做登录认证，不校验权限
     *
     * @return
     */
    protected String[] permitted() {
        return new String[]{};
    }

    /**
     * 此数组中定义的路径，不会被Spring Security 拦截
     *
     * @return
     */
    protected String[] ignored() {
        return new String[]{};
    }

    /**
     * 设置SpringSecurity 允许的请求头
     *
     * @return
     */
    protected List<Header> headerWrite() {
        return Arrays.asList(
//                new Header("Access-control-Allow-Origin","*"),
                new Header("Access-Control-Expose-Headers", "Authorization"));
    }
}
