package com.irises.framework.security.userdetails;

import com.irises.framework.security.core.IrisesGrantedAuthority;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.ArrayList;
import java.util.List;


/**
 * 从数据库加载用户信息
 */
public class IrisesUserDetailsService implements UserDetailsService {

    private IrisesSecurityService irisesSecurityService;

    public IrisesUserDetailsService(IrisesSecurityService irisesSecurityService) {
        this.irisesSecurityService = irisesSecurityService;
    }

    /**
     * 从数据库获取用户信息及权限
     * @param userName
     * @return
     * @throws UsernameNotFoundException
     */
    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        IrisesUser irisesUser = irisesSecurityService.getIrisesUserDetails(userName);
        if(null != irisesUser){
            List<IrisesRule> irisesResources = irisesSecurityService.getResourcesByUserId(irisesUser.getId());
            List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
            for (IrisesRule irisesResource : irisesResources) {
                if (null != irisesResource && null != irisesResource.getCode()) {
                    //资源名称、资源请求方式 构成一个权限对象
                    GrantedAuthority nameAuthority = new IrisesGrantedAuthority(irisesResource.getCode(), irisesResource.getMethod());
                    grantedAuthorities.add(nameAuthority);
                }
            }
            return new SecurityUser(irisesUser.getId(),irisesUser.getIdentifier(), irisesUser.getCredential(), irisesUser.isEnabled(), grantedAuthorities);
        }else {
            throw new UsernameNotFoundException("此账号不存在!");
        }
    }
}
