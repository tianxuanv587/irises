package com.irises.framework.security.web.sms;

import com.irises.framework.web.http.WebResponse;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 描述： 手机登录短信发送
 * </p>
 *
 * @author 田轩
 * @since 2020/11/3
 */
public abstract class AbstractSecuritySmsController {
    /**
     * 发送短信验证码
     * @return
     */
    @PostMapping("/sms/code")
    public abstract WebResponse sendSms(@RequestBody SmsAO smsAO, HttpServletRequest request);
}
