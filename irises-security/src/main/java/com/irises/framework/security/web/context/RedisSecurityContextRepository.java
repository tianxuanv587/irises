package com.irises.framework.security.web.context;

import com.irises.framework.security.constant.Constants;
import com.irises.framework.security.core.IrisesGrantedAuthority;
import com.irises.framework.security.support.IrisesSecurityUtils;
import com.irises.framework.security.userdetails.SecurityUser;
import com.irises.framework.tools.json.Jackson;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.security.authentication.AuthenticationTrustResolver;
import org.springframework.security.authentication.AuthenticationTrustResolverImpl;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.security.web.context.HttpRequestResponseHolder;
import org.springframework.security.web.context.SaveContextOnUpdateOrErrorResponseWrapper;
import org.springframework.security.web.context.SecurityContextRepository;
import org.springframework.util.ClassUtils;
import org.springframework.web.util.WebUtils;

import javax.servlet.AsyncContext;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * 将SecurityContext 存储进redis,不再依赖session
 */

public class RedisSecurityContextRepository implements SecurityContextRepository {
    protected final Log logger = LogFactory.getLog(this.getClass());
    private boolean disableUrlRewriting = false;
    private boolean isServlet3 = ClassUtils.hasMethod(ServletRequest.class, "startAsync", new Class[0]);
    private AuthenticationTrustResolver trustResolver = new AuthenticationTrustResolverImpl();

    private StringRedisTemplate redisTemplate;

    public RedisSecurityContextRepository(StringRedisTemplate redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    @Override
    public SecurityContext loadContext(HttpRequestResponseHolder requestResponseHolder) {
        HttpServletRequest request = requestResponseHolder.getRequest();
        HttpServletResponse response = requestResponseHolder.getResponse();
        SecurityContext context = this.readSecurityContextFromRedis(request);
        if (context == null) {
            if (this.logger.isDebugEnabled()) {
                this.logger.debug("No SecurityContext was available from the Redis. A new one will be created.");
            }

            context = this.generateNewContext();
        }
        RedisSecurityContextRepository.SaveToRedisResponseWrapper wrappedResponse = new RedisSecurityContextRepository.SaveToRedisResponseWrapper(response, request, context);
        requestResponseHolder.setResponse(wrappedResponse);
        if (this.isServlet3) {
            requestResponseHolder.setRequest(new RedisSecurityContextRepository.Servlet3SaveToRedisRequestWrapper(request, wrappedResponse));
        }
        return context;
    }

    @Override
    public void saveContext(SecurityContext context, HttpServletRequest request, HttpServletResponse response) {
        SaveToRedisResponseWrapper responseWrapper = (SaveToRedisResponseWrapper)WebUtils.getNativeResponse(response, SaveContextOnUpdateOrErrorResponseWrapper.class);
        if (responseWrapper == null) {
            throw new IllegalStateException("Cannot invoke saveContext on response " + response + ". You must use the HttpRequestResponseHolder.response after invoking loadContext");
        } else {
            if (!responseWrapper.isContextSaved()) {
                responseWrapper.saveContext(context);
            }

        }
    }

    @Override
    public boolean containsContext(HttpServletRequest httpServletRequest) {
        String contextKey = IrisesSecurityUtils.getContextKey(httpServletRequest);
        return redisTemplate.hasKey(contextKey);
    }

    private SecurityContext readSecurityContextFromRedis(HttpServletRequest request) {
        boolean debug = this.logger.isDebugEnabled();
        if (request == null) {
            if (debug) {
                this.logger.debug("No request currently exists");
            }
            return null;
        } else {
            String contextKey = IrisesSecurityUtils.getContextKey(request);
            String o = redisTemplate.opsForValue().get(contextKey);
//            Map contextFromRedis = JSONUtil.parseObj(o).toBean(Map.class);
            Map contextFromRedis = Jackson.jsonParseObject(o, Map.class);
            if (contextFromRedis == null || contextFromRedis.size() == 0) {
                if (debug) {
                    this.logger.debug("Redis returned null object for SPRING_SECURITY_CONTEXT");
                }

                return null;
            }  else {
                if (debug) {
                    this.logger.debug("Obtained a valid SecurityContext from " + contextKey + ": '" + contextFromRedis + "'");
                }
                // context被访问过后更新过期时间(默认2小时不使用该Context则清除)
                long timeout = IrisesSecurityUtils.getContextKeyTimeout(request);
                redisTemplate.expire(contextKey, timeout, TimeUnit.MINUTES);

                Map authenticationMap = (Map)contextFromRedis.get("authentication");
                Object credentials = authenticationMap.get("credentials");
                List<GrantedAuthority> authorities = new ArrayList<>();
                List<Map<String,String>> authoritiesList = ( List<Map<String,String>>)authenticationMap.get("authorities");
                for(Map<String,String> authorityMap : authoritiesList){
                    String[] authorityArray = authorityMap.get("authority").split(":");
                    GrantedAuthority authority = new IrisesGrantedAuthority(authorityArray[0], authorityArray[1]);
                    authorities.add(authority);
                }
                Map principalMap = (Map)authenticationMap.get("principal");
                String username = (String)principalMap.get("username");
                String password = (String)principalMap.get("password");
                Boolean accountNonExpired = (Boolean)principalMap.get("accountNonExpired");
                Boolean accountNonLocked = (Boolean)principalMap.get("accountNonLocked");
                Boolean credentialsNonExpired = (Boolean)principalMap.get("credentialsNonExpired");
                Boolean enabled = (Boolean)principalMap.get("enabled");
                Long id = Long.valueOf(principalMap.get("id").toString());

                SecurityUser securityUser = new SecurityUser(id,username, null == password ? "protected" : password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
                SecurityContextImpl securityContext = new SecurityContextImpl();
                UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(securityUser, credentials, authorities);
                securityContext.setAuthentication(authentication);
                return securityContext;
            }
        }
    }

    protected SecurityContext generateNewContext() {
        return SecurityContextHolder.createEmptyContext();
    }


    final class SaveToRedisResponseWrapper extends SaveContextOnUpdateOrErrorResponseWrapper {
        private final HttpServletRequest request;
        private final SecurityContext contextBeforeExecution;
        private final Authentication authBeforeExecution;

        SaveToRedisResponseWrapper(HttpServletResponse response, HttpServletRequest request, SecurityContext context) {
            super(response, RedisSecurityContextRepository.this.disableUrlRewriting);
            this.request = request;
            this.contextBeforeExecution = context;
            this.authBeforeExecution = context.getAuthentication();
        }
        @Override
        protected void saveContext(SecurityContext context) {
            Authentication authentication = context.getAuthentication();
            if (authentication != null && !RedisSecurityContextRepository.this.trustResolver.isAnonymous(authentication)) {
                if (this.contextChanged(context)) {
                    String contextKey = IrisesSecurityUtils.getContextKey(this.request);

                    long timeout = IrisesSecurityUtils.getContextKeyTimeout(request);
                    redisTemplate.opsForValue().set(contextKey,Jackson.toJSONString(context), timeout, TimeUnit.MINUTES);
                    if (RedisSecurityContextRepository.this.logger.isDebugEnabled()) {
                        RedisSecurityContextRepository.this.logger.debug("SecurityContext '" + context + "' stored to Redis'");
                    }
                }

            } else {
                if (RedisSecurityContextRepository.this.logger.isDebugEnabled()) {
                    RedisSecurityContextRepository.this.logger.debug("SecurityContext is empty or contents are anonymous - context will not be stored in HttpSession.");
                }
            }
        }

        private boolean contextChanged(SecurityContext context) {
            return context != this.contextBeforeExecution || context.getAuthentication() != this.authBeforeExecution;
        }

    }

    private static class Servlet3SaveToRedisRequestWrapper extends HttpServletRequestWrapper {
        private final SaveContextOnUpdateOrErrorResponseWrapper response;

        public Servlet3SaveToRedisRequestWrapper(HttpServletRequest request, SaveContextOnUpdateOrErrorResponseWrapper response) {
            super(request);
            this.response = response;
        }

        public AsyncContext startAsync() {
            this.response.disableSaveOnResponseCommitted();
            return super.startAsync();
        }

        public AsyncContext startAsync(ServletRequest servletRequest, ServletResponse servletResponse) throws IllegalStateException {
            this.response.disableSaveOnResponseCommitted();
            return super.startAsync(servletRequest, servletResponse);
        }
    }

     /**
     * 根据用户Id从redis中删除context
     * @param userIds
     */
    public void cleanContextByUserId(List<Long> userIds){
        if(null != userIds){
            Set<String> contextKeys = new HashSet<>();
            for(Long userId : userIds){
                Set<String> keys = redisTemplate.keys(Constants.SPRING_SECURITY_CONTEXT_KEY + ":" + userId + "*");
                contextKeys.addAll(keys);
            }
            redisTemplate.delete(contextKeys);
        }
    }
}
