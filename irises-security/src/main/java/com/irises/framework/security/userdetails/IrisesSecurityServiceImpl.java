package com.irises.framework.security.userdetails;

import com.irises.framework.security.dubboservice.IrisesDubboSecurityService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 描述： 此层为了解耦，如果实现方式变化之后避免影响
 * </p>
 *
 * @author 田轩
 * @since 2021/4/7
 */
public class IrisesSecurityServiceImpl implements IrisesSecurityService<IrisesRule,IrisesUser> {
    private static List<IrisesRule> empty_list = new ArrayList<>();

    private IrisesDubboSecurityService irisesDubboSecurityService;

    public IrisesSecurityServiceImpl(IrisesDubboSecurityService irisesDubboSecurityService) {
        this.irisesDubboSecurityService = irisesDubboSecurityService;
    }

    @Override
    public List<IrisesRule> getResources() {
        return irisesDubboSecurityService.getResources();
    }

    @Override
    public List<IrisesRule> getResourcesByUserId(Long userId) {
        return irisesDubboSecurityService.getResourcesByUserId(userId);
    }

    @Override
    public IrisesUser getIrisesUserDetails(String identifier) {
        IrisesUser irisesUser = irisesDubboSecurityService.getIrisesUserDetails(identifier);
        return irisesUser;
    }
}
