package com.irises.framework.security.exception;

import org.springframework.security.core.AuthenticationException;

/**
 * <p>
 * 描述：验证码校验失败
 * </p>
 *
 * @author 田轩
 * @since 2020/10/30
 */
public class ValidateCodeException extends AuthenticationException {

    public ValidateCodeException(String msg) {
        super(msg);
    }
}
