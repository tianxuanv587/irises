package com.irises.framework.security.web.authentication;


import cn.hutool.json.JSONUtil;
import com.irises.framework.security.constant.Constants;
import com.irises.framework.web.http.WebResponse;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

/**
 * 认证成功处理器
 */
public class IrisesRedisAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

    private StringRedisTemplate redisTemplate;

    public IrisesRedisAuthenticationSuccessHandler(StringRedisTemplate redisTemplate){
        this.redisTemplate = redisTemplate;
    }

    @Override
    public void onAuthenticationSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) throws IOException, ServletException {
        //生成token
        String userId = String.valueOf(httpServletRequest.getAttribute(Constants.SPRING_SECURITY_CONTEXT_USER_KEY));
        String username = String.valueOf(httpServletRequest.getAttribute(Constants.SPRING_SECURITY_USERNAME_KEY));
        String tokenId = String.valueOf(httpServletRequest.getAttribute(Constants.SPRING_SECURITY_CONTEXT_TOKEN_ID));
        String loginModel = String.valueOf(httpServletRequest.getAttribute(Constants.SPRING_SECURITY_LOGIN_MODE_KEY));

        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MINUTE, Constants.JWT_CLIENT_EXPIRE_TIME);
        Date expiration = calendar.getTime();
        String token = Jwts.builder()
                .setClaims(new HashMap(){{
                    put(Constants.SPRING_SECURITY_CONTEXT_USER_KEY, userId);
                    put(Constants.SPRING_SECURITY_CONTEXT_TOKEN_ID, tokenId);
                    put(Constants.SPRING_SECURITY_USERNAME_KEY, username);
                    put(Constants.SPRING_SECURITY_LOGIN_MODE_KEY, loginModel);
                }})
                .setIssuedAt(new Date())
                .signWith(SignatureAlgorithm.HS512, Constants.JWT_SECRET_CIPHER)
                .setExpiration(expiration)
                .compact();
        WebResponse response = WebResponse.success(new HashMap<String,String>(){{put("token",token);}});
        httpServletResponse.setStatus(HttpServletResponse.SC_OK);
        httpServletResponse.setContentType(MediaType.APPLICATION_JSON_VALUE);
        httpServletResponse.setCharacterEncoding(StandardCharsets.UTF_8.toString());
        httpServletResponse.setHeader (Constants.SPRING_SECURITY_HEADER_NAME,token);
        httpServletResponse.getWriter().write(JSONUtil.toJsonStr(response));
    }

}