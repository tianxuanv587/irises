package com.irises.framework.security.web.authentication;

import cn.hutool.json.JSONUtil;
import com.irises.framework.security.constant.Constants;
import com.irises.framework.security.userdetails.SecurityUser;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.util.StreamUtils;
import org.springframework.util.StringUtils;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Map;
import java.util.UUID;

public class IrisesSessionUsernamePasswordAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    public IrisesSessionUsernamePasswordAuthenticationFilter(AuthenticationManager authenticationManager, AuthenticationSuccessHandler successHandler, AuthenticationFailureHandler failureHandler) {
        this.setAuthenticationSuccessHandler(successHandler);
        this.setAuthenticationFailureHandler(failureHandler);
        this.setAuthenticationManager(authenticationManager);
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
        String body;
        try {
            body = StreamUtils.copyToString(request.getInputStream(), Charset.forName("UTF-8"));
        } catch (Exception e) {
            throw new AuthenticationServiceException("登录错误");
        }
        String username = null, password = null, mode = null;
        if(StringUtils.hasText(body)) {
            Map<String,String> params = JSONUtil.parseObj(body).toBean(Map.class);
            username = params.get("username");
            password = params.get("password");
            mode = params.get("mode");
        }
        request.setAttribute(SPRING_SECURITY_FORM_USERNAME_KEY, username);
        request.setAttribute(SPRING_SECURITY_FORM_PASSWORD_KEY, password);
        request.setAttribute(Constants.SPRING_SECURITY_LOGIN_MODE_KEY, mode);
        return super.attemptAuthentication(request,response);
    }


    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) throws IOException, ServletException {
        SecurityUser user =(SecurityUser)authResult.getPrincipal();
        request.setAttribute(Constants.SPRING_SECURITY_CONTEXT_USER_KEY,user.getId());
        request.setAttribute(Constants.SPRING_SECURITY_CONTEXT_TOKEN_ID,UUID.randomUUID().toString());
        super.successfulAuthentication(request, response, chain, authResult);
    }

    @Override
    protected String obtainPassword(HttpServletRequest request) {
        return (String)request.getAttribute(SPRING_SECURITY_FORM_PASSWORD_KEY);
    }

    @Override
    protected String obtainUsername(HttpServletRequest request) {
        return (String)request.getAttribute(SPRING_SECURITY_FORM_USERNAME_KEY);
    }
}
