package com.irises.framework.security.support;

import cn.hutool.core.util.RandomUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.irises.framework.security.constant.Constants;
import com.irises.framework.security.exception.ValidateCodeException;
import com.irises.framework.security.model.IrisesValidateCode;
import com.irises.framework.tools.json.Jackson;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.util.StringUtils;

import java.util.concurrent.TimeUnit;

/**
 * <p>
 * 描述： 使用redis存储验证的实现
 * </p>
 *
 * @author 田轩
 * @since 2020/10/31
 */
public class RedisValidateCodeSupport {

    private StringRedisTemplate redisTemplate;

    public RedisValidateCodeSupport(StringRedisTemplate redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    public String generateCode(String mobile) {
        if(StringUtils.isEmpty(mobile)) throw new ValidateCodeException("手机号不能为空！");
        StringBuffer code = new StringBuffer();
        for(int i = 0; i < 6; i++) {
            code.append(RandomUtil.randomInt(0, 9));
        }
        // 将验证码存入到Redis, 过期时间5分钟
        IrisesValidateCode validateCode = new IrisesValidateCode(code.toString(), 60 * 5);
        redisTemplate.opsForValue().set(Constants.STRING_SECURITY_SMS_CODE_KEY_PREFIX + mobile, Jackson.toJSONString(validateCode), 60 * 5, TimeUnit.SECONDS);
        return code.toString();
    }

    public void validate(String mobile, String codeInRequest) {
        if(!StringUtils.hasText(codeInRequest)){
            throw new ValidateCodeException("验证码不能为空！");
        }

        String key = Constants.STRING_SECURITY_SMS_CODE_KEY_PREFIX + mobile;
        // 从Redis中获取登录验证码
        String validateCodeStr = redisTemplate.opsForValue().get(key);

        if(null == validateCodeStr){
            throw new ValidateCodeException("验证码已过期！");
        }

        JSONObject jsonObject = JSONUtil.parseObj(validateCodeStr);
        IrisesValidateCode validateCode = null != jsonObject ? jsonObject.toBean(IrisesValidateCode.class) : null;

        if(!validateCode.getCode().equals(codeInRequest)) {
            throw new ValidateCodeException("验证码不正确！");
        }
        //从Redis中删除验证码
        redisTemplate.delete(key);
    }
}
