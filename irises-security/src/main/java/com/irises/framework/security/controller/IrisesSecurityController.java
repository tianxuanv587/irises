package com.irises.framework.security.controller;

import com.irises.framework.thread.ThreadLocalHolder;
import com.irises.framework.tools.json.Jackson;
import com.irises.framework.web.mvc.IrisesController;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.ModelAttribute;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * <p>
 * 描述：
 * </p>
 *
 * @author 田轩
 * @since 2021/4/12
 */
public abstract class IrisesSecurityController extends IrisesController {

    @ModelAttribute
    public void init(HttpServletRequest request){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if(null != authentication) {
            Object principal = authentication.getPrincipal();
            if(null != principal && !"anonymousUser".equals(principal)) {
                Map map = Jackson.jsonParseObject(Jackson.toJSONString(principal), Map.class);
                String userId = String.valueOf(map.get("id"));
                ThreadLocalHolder.userThreadLocal.set(Long.valueOf(userId));
            }
        }
    }
}
