package com.irises.framework.security.support;

import com.irises.framework.security.constant.Constants;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;

public class IrisesSecurityUtils {
    private IrisesSecurityUtils(){}

    public static String getContextKey(HttpServletRequest request){
        Object userId = request.getAttribute(Constants.SPRING_SECURITY_CONTEXT_USER_KEY);
        Object tokenId = request.getAttribute(Constants.SPRING_SECURITY_CONTEXT_TOKEN_ID);
        String contextKey = Constants.SPRING_SECURITY_CONTEXT_KEY + ":" +  userId + ":" + tokenId;
        return contextKey;
    }

    /**
     * 获取过期时间
     * @param request
     * @return
     */
    public static long getContextKeyTimeout(HttpServletRequest request){
        String model = (String)request.getAttribute(Constants.SPRING_SECURITY_LOGIN_MODE_KEY);
        boolean loginModelFlag =  StringUtils.hasText(model) && Constants.SPRING_SECURITY_LOGIN_MODE_LONG.equals(model);
        long timeout =   loginModelFlag ? Constants.SPRING_SECURITY_KEY_EXPIRES_LONG_TIME : Constants.SPRING_SECURITY_KEY_EXPIRES_SHORT_TIME;
        return timeout;
    }
}
