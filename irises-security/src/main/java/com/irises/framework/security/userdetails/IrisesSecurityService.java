package com.irises.framework.security.userdetails;

import java.util.List;

/**
 * <p>
 * 此接口为springSecurity返回用户账号、及用户权限
 *  此接口请勿作为业务接口调用
 * </p>
 *
 * @author 田轩
 * @since 2020/10/16
 */
public interface IrisesSecurityService<R extends IrisesRule, U extends IrisesUser> {
    /**
     * 获取所有的受限资源，将会被Security框架拦截
     * @return
     */
    List<R> getResources();

    /**
     * 获取某个用户所拥有的权限
     * @param userId
     * @return
     */
    List<R> getResourcesByUserId(Long userId);

    /**
     * 获取用户信息及
     * @param identifier
     * @return
     */
    U getIrisesUserDetails(String identifier);
}
