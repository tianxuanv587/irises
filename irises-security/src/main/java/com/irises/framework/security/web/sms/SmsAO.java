package com.irises.framework.security.web.sms;

/**
 * <p>
 * 描述：
 * </p>
 *
 * @author 田轩
 * @since 2020/11/4
 */
public class SmsAO {
    private String mobile;

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
}
