package com.irises.framework.mybatisplus.entity;


import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
public class IrisesEntity implements Serializable {
    /**
     * 主键
     */
    @JsonSerialize(using= ToStringSerializer.class)
    protected Long id;

    /**
     * 创建日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.INSERT)
    protected LocalDateTime createTime;

    /**
     * 最后修改日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    protected LocalDateTime modifiedTime;

    /**
     * 创建者
     */
    @TableField(fill = FieldFill.INSERT)
    @JsonSerialize(using= ToStringSerializer.class)
    private Long creator;

    /**
     * 修改者
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    @JsonSerialize(using= ToStringSerializer.class)
    private Long modifier;

    @TableField(fill = FieldFill.INSERT)
    @TableLogic
    protected int deleted;  //是否删除  0 : 未删除  1： 删除
}
