package com.irises.framework.mybatisplus.handler;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.baomidou.mybatisplus.core.handlers.StrictFill;
import com.baomidou.mybatisplus.core.metadata.TableInfo;
import com.irises.framework.thread.ThreadLocalHolder;
import org.apache.ibatis.reflection.MetaObject;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.function.Supplier;

public class MyMetaObjectHandler implements MetaObjectHandler {

    @Override
    public void insertFill(MetaObject metaObject) {
        this.strictInsertFill(metaObject,"deleted", Boolean.class,false);
        this.strictInsertFill(metaObject,"createTime",LocalDateTime.class, LocalDateTime.now());
        this.strictInsertFill(metaObject,"modifiedTime", LocalDateTime.class, LocalDateTime.now());
        Long userId = ThreadLocalHolder.userThreadLocal.get();
        this.strictInsertFill(metaObject,"creator",Long.class, userId);
        this.strictInsertFill(metaObject,"modifier",Long.class, userId);
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        this.strictUpdateFill(metaObject, "modifiedTime", LocalDateTime.class, LocalDateTime.now());
        Long userId = ThreadLocalHolder.userThreadLocal.get();
        this.strictUpdateFill(metaObject,"modifier", Long.class, userId);
    }

    @Override
    public MetaObjectHandler strictFillStrategy(MetaObject metaObject, String fieldName, Supplier<?> fieldVal) {
        //需要自动填充的，无论是否有值，强制进行填充
//        if (metaObject.getValue(fieldName) == null) {
            Object obj = fieldVal.get();
            if (Objects.nonNull(obj)) {
                metaObject.setValue(fieldName, obj);
            }
//        }
        return this;
    }
}