package com.irises.framework.web.binder;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.irises.framework.web.binder.annotation.PageBinder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

/**
 *  分页参数绑定
 */
@Slf4j
public class PageBinderArgumentResolver implements HandlerMethodArgumentResolver {
    @Override
    public boolean supportsParameter(MethodParameter methodParameter) {
        return methodParameter.hasParameterAnnotation(PageBinder.class);
    }

    @Override
    public Object resolveArgument(MethodParameter methodParameter, ModelAndViewContainer modelAndViewContainer, NativeWebRequest nativeWebRequest, WebDataBinderFactory webDataBinderFactory) throws Exception {
        Page pageable = null;
        String pageName = methodParameter.getParameterAnnotation(PageBinder.class).page();
        String limitName = methodParameter.getParameterAnnotation(PageBinder.class).limit();
        try{
            long page = Long.parseLong(nativeWebRequest.getParameter(pageName));
            long limit = Long.parseLong(nativeWebRequest.getParameter(limitName));
            pageable = new Page(page,limit);
        }catch (Exception e){
            log.debug("无分页参数,查询所有数据");
        }
        return pageable;
    }
}