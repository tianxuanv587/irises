package com.irises.framework.web.binder.annotation;

import java.lang.annotation.*;

@Target({ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface PageBinder {
    String page() default "page";
    String limit() default "limit";
}