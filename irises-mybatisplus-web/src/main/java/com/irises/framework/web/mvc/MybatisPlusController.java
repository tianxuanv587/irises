package com.irises.framework.web.mvc;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.irises.framework.web.binder.annotation.PageBinder;
import com.irises.framework.web.binder.annotation.QueryBinder;
import com.irises.framework.web.http.WebResponse;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

/**
 * @author tianxuan
 */
public abstract class MybatisPlusController<S extends IService<T>, T> extends IrisesController {

    @Autowired
    protected S baseService;

    /*@ModelAttribute
    public void init(HttpServletRequest request){

    }*/

    protected S getBaseService() {
        return this.baseService;
    }

    @ApiOperation("添加")
    @ApiImplicitParam(
            name = "Authorization",
            value = "token",
            paramType = "header",
            dataType = "String"
    )
    @PostMapping
    protected WebResponse create(@RequestBody T entity){
        baseService.save(entity);
        return WebResponse.success();
    }

    @ApiOperation("更新")
    @ApiImplicitParam(
            name = "Authorization",
            value = "token",
            paramType = "header",
            dataType = "String"
    )
    @PutMapping
    protected WebResponse update(@RequestBody T entity){
        baseService.updateById(entity);
        return WebResponse.success();
    }

    @ApiOperation("删除")
    @ApiImplicitParam(
            name = "Authorization",
            value = "token",
            paramType = "header",
            dataType = "String"
    )
    @DeleteMapping("/{id}")
    protected WebResponse delete(@PathVariable("id") Long id){
        baseService.removeById(id);
        return WebResponse.success();
    }

    @ApiOperation("详情")
    @ApiImplicitParam(
            name = "Authorization",
            value = "token",
            paramType = "header",
            dataType = "String"
    )
    @GetMapping("/{id}")
    protected WebResponse detail(@PathVariable("id") Long id){
        T entity = baseService.getById(id);
        return WebResponse.success(entity);
    }

    @ApiOperation("列表")
    @ApiImplicitParam(
            name = "Authorization",
            value = "token",
            paramType = "header",
            dataType = "String"
    )
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "当前页码", paramType = "query", dataType = "Integer"),
            @ApiImplicitParam(name = "limit", value = "每页行数", paramType = "query",  dataType = "Integer")
    })
    @GetMapping
    protected WebResponse list(@ApiIgnore @PageBinder Page page,@ApiIgnore @QueryBinder QueryWrapper<T> query){
        WebResponse webResponse;
        if(null != page){
            IPage<T> pageData = baseService.page(page, query);
            webResponse = WebResponse.success(pageData);
        }else {
            List<T> listData = baseService.list(query);
            webResponse = WebResponse.success(listData);
        }
        return webResponse;
    }
}
