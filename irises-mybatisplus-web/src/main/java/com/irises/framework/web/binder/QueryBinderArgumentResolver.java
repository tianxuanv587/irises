package com.irises.framework.web.binder;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.irises.framework.web.binder.annotation.QueryBinder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.MethodParameter;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import java.util.Map;

/**
 *  分页参数绑定
 */
@Slf4j
public class QueryBinderArgumentResolver implements HandlerMethodArgumentResolver {
    @Override
    public boolean supportsParameter(MethodParameter methodParameter) {
        return methodParameter.hasParameterAnnotation(QueryBinder.class);
    }

    @Override
    public Object resolveArgument(MethodParameter methodParameter, ModelAndViewContainer modelAndViewContainer, NativeWebRequest nativeWebRequest, WebDataBinderFactory webDataBinderFactory) throws Exception {
        QueryWrapper queryWrapper = new QueryWrapper();
        Map<String, String[]> parameterMap = nativeWebRequest.getParameterMap();
        if(null != parameterMap && parameterMap.size() > 0){
            parameterMap.forEach((key,values) -> {
                if(key.startsWith("search_") && null !=values && values.length > 0 && StringUtils.hasText(values[0])){
                    String[] queryParameter = key.split("_");
                    if(queryParameter.length > 2){
                        String queryParameterName = queryParameter[1];
                        String queryOperator = queryParameter[2];
                        switch (queryOperator){
                            case "eq" :  queryWrapper.eq(queryParameterName,values[0]);break;
                            case "ne" :  queryWrapper.ne(queryParameterName,values[0]);break;
                            case "gt" :  queryWrapper.gt(queryParameterName,values[0]);break;
                            case "ge" :  queryWrapper.ge(queryParameterName,values[0]);break;
                            case "lt" :   queryWrapper.lt(queryParameterName,values[0]);break;
                            case "le" :   queryWrapper.le(queryParameterName,values[0]);break;
                            case "like" :   queryWrapper.like(queryParameterName,values[0]);break;
                            case "likeR" :   queryWrapper.likeRight(queryParameterName,values[0]);break;
                            case "likeL" :   queryWrapper.likeLeft(queryParameterName,values[0]);break;
                            case "isNull" :   queryWrapper.isNull(values[0]);break;
                            case "notNull" :   queryWrapper.isNotNull(values[0]);break;
                        }
                    }
                }else if(key.startsWith("order_")){
                    String[] orderParameter = key.split("_");
                    if(orderParameter.length > 1){
                        String orderParameterName = orderParameter[1];
                        if(StringUtils.isEmpty(values) || "asc".equals(values[0])){
                            queryWrapper.orderByAsc(orderParameterName);
                        }else{
                            queryWrapper.orderByDesc(orderParameterName);
                        }
                    }
                }
            });
        }
        return queryWrapper;
    }


}