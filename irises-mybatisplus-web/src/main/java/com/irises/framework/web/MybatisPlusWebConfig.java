package com.irises.framework.web;


import com.irises.framework.web.binder.PageBinderArgumentResolver;
import com.irises.framework.web.binder.QueryBinderArgumentResolver;
import com.irises.framework.web.configuration.IrisesWebConfig;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;

import java.util.List;

@Configuration
public class MybatisPlusWebConfig extends IrisesWebConfig {
    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
        argumentResolvers.add(new PageBinderArgumentResolver());
        argumentResolvers.add(new QueryBinderArgumentResolver());
    }
}
