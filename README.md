# 鸢尾花

#### 介绍
基于SpringBoot、spring cloud、spring cloud Alibaba项目提取的一套公共组件，用于屏蔽各项依赖与通用配置，简化业务框架配置复杂度。

#### 软件结构
模块 | 当前版本 | 描述
---|---|---
irises-commons | v2.0.1 | 聚合工具包、各模块通用依赖
irises-web | v2.0.1 | 提取了springboot的web配置类、请求响应对象和通用响应业务状态码，并集成swagger2
irises-mybatisplus | v2.0.2 | 分页插件、自动注入、BaseEntity、Druid
irises-mybatisplus-web | v2.0.1 | 基于springboot的web和Mybatis-plus实现了对查询请求的简单条件解析自动参数绑定
irises-redis | v2.0.1 | 提取了redis配置类
irises-security | v2.1 | 基于springSecurity封装了基于Redis和基于Session两种模式的自定义认证与授权



#### 使用文档

https://blog.csdn.net/cruel_xuan/article/details/115719878

##### 版权 | License
Apache License 2.0

#### 目的
期望大佬指点