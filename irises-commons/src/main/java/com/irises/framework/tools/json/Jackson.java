package com.irises.framework.tools.json;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

/**
 * <p>
 * 描述：
 * </p>
 *
 * @author 田轩
 * @since 2020/10/17
 */
public class Jackson {
    private static final ObjectMapper objectMapper = new ObjectMapper();
    private static final XmlMapper xmlMapper = new XmlMapper();
    private Jackson(){}

    public  static String toJSONString(Object o){
        try {
            return objectMapper.writeValueAsString(o);
        } catch (JsonProcessingException e) {
            return null;
        }
    }

    public static <T> T jsonParseObject(String json,Class<T> tClass) {
        try {
            return objectMapper.readValue(json, tClass);
        } catch (Exception e) {
            return null;
        }
    }

    public static  String toXmlString(Object o) {
        try{
            return xmlMapper.writeValueAsString(o);
        }catch (Exception e) {
            return null;
        }
    }

    public static  <T> T xmlParseObject(String xml, Class<T> tClass){
        try{
            return xmlMapper.readValue(xml, tClass);
        }catch (Exception e) {
            return null;
        }
    }
}
