package com.irises.framework.redis;

import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.irises.framework.redis.model.RedisLock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.Cursor;
import org.springframework.data.redis.core.ScanOptions;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.Arrays;
import java.util.HashSet;
import java.util.concurrent.TimeUnit;

public class StringRedisRepository {
    private static final Logger log = LoggerFactory.getLogger(StringRedisRepository.class);
    private static final int SCAN_KEYS_LIMIT = 10000;  //查询redisKey的时的上限
    private StringRedisTemplate redisTemplate;

    public StringRedisRepository(StringRedisTemplate redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    //=============================common============================
    /**
     * 指定缓存失效时间
     * @param key 键
     * @param time 时间(秒)
     * @return
     */
    public boolean expire(String key,long time){
       return expire(key,time, TimeUnit.SECONDS);
    }

    /**
     * 指定缓存失效时间
     * @param key 键
     * @param time 时间(秒)
     * @return
     */
    public boolean expire(String key,long time, TimeUnit timeUnit){
        try {
            if(time>0){
                redisTemplate.expire(key, time, timeUnit);
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 根据key 获取过期时间
     * @param key 键 不能为null
     * @return 时间(分钟) 返回0代表为永久有效
     */
    public long getExpire(String key){
        return getExpire(key,TimeUnit.SECONDS);
    }

    /**
     * 根据key 获取过期时间
     * @param key 键 不能为null
     * @return 时间(分钟) 返回0代表为永久有效
     */
    public long getExpire(String key, TimeUnit timeUnit){
        return redisTemplate.getExpire(key,timeUnit);
    }

    /**
     * 判断key是否存在
     * @param key 键
     * @return true 存在 false不存在
     */
    public boolean hasKey(String key){
        try {
            return redisTemplate.hasKey(key);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 删除缓存
     * @param key 可以传一个值 或多个
     */
    @SuppressWarnings("unchecked")
    public void del(String ... key){
        if(key!=null&&key.length>0){
            if(key.length==1){
                redisTemplate.delete(key[0]);
            }else{
                redisTemplate.delete(CollectionUtils.arrayToList(key));
            }
        }
    }

    //============================String=============================
    /**
     * 普通缓存获取
     * @param key 键
     * @return 值
     */
    public <T> T get(String key,Class<T> clazz){
        T t = null;
        if(StringUtils.hasText(key)){
            String o = redisTemplate.opsForValue().get(key);
            JSONObject jsonObject = JSONUtil.parseObj(o);
             t = null != jsonObject ? jsonObject.toBean(clazz) : null;
        }
        return t;
    }
    /**
     * 普通缓存获取
     * @param key 键
     * @return 值
     */
    public String get(String key){
          return redisTemplate.opsForValue().get(key);
    }

    public HashSet scanKeys(String keyPattern){
        return scanKeys(keyPattern, SCAN_KEYS_LIMIT);
    }

    public  HashSet scanKeys(String keyPattern, int limit){
        RedisConnection rc = null;
        ScanOptions options = ScanOptions.scanOptions().match(keyPattern).count(limit).build();
        HashSet<String> set = new HashSet<>();
        try {
            RedisConnectionFactory factory = redisTemplate.getConnectionFactory();
            rc = factory.getConnection();
            Cursor<byte[]> cursor = rc.scan(options);
            while (cursor.hasNext()) {
                set.add(new String(cursor.next()));
            }
        } catch (Exception e) {

        }finally {
            if (rc != null) {
                rc.close();
            }
        }
        return set;
    }

    /**
     * 普通缓存放入
     * @param key 键
     * @param value 值
     * @return true成功 false失败
     */
    public boolean set(String key,Object value) {
        try {
            if(value instanceof  String) {
                redisTemplate.opsForValue().set(key,(String)value);
            }else {
                redisTemplate.opsForValue().set(key, JSONUtil.toJsonStr(value));
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }


    /**
     * 普通缓存放入并设置时间
     * @param key 键
     * @param value 值
     * @param time 时间(分钟) time要大于0 如果time小于等于0 将设置无限期
     * @return true成功 false 失败
     */
    public boolean set(String key,Object value,long time){
       return set(key, value, time, TimeUnit.SECONDS);
    }

    /**
     * 普通缓存放入并设置时间
     * @param key 键
     * @param value 值
     * @param time 时间(分钟) time要大于0 如果time小于等于0 将设置无限期
     * @return true成功 false 失败
     */
    public boolean set(String key,Object value,long time, TimeUnit timeUnit){
        try {
            if(time>0){
                if(value instanceof  String) {
                    redisTemplate.opsForValue().set(key,(String)value, time, timeUnit);
                }else {
                    redisTemplate.opsForValue().set(key, JSONUtil.toJsonStr(value), time,timeUnit);
                }
            }else{
                set(key, value);
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 加锁
     *
     * @param redisLockEntity 锁实体
     * @return
     */
    public boolean lock(RedisLock redisLockEntity) {
        return redisTemplate.opsForValue().setIfAbsent(redisLockEntity.getLockKey(), redisLockEntity.getRequestId(), 2, TimeUnit.MINUTES);
    }

    /**
     * 非原子解锁，可能解别人锁，不安全
     *
     * @param redisLockEntity
     * @return
     */
    public boolean unlock(RedisLock redisLockEntity) {
        if (redisLockEntity == null || redisLockEntity.getLockKey() == null || redisLockEntity.getRequestId() == null) {
            return false;
        }
        boolean releaseLock = false;
        String requestId = (String) redisTemplate.opsForValue().get(redisLockEntity.getLockKey());
        if (redisLockEntity.getRequestId().equals(requestId)) {
            releaseLock = redisTemplate.delete(redisLockEntity.getLockKey());
        }
        return releaseLock;
    }

    /**
     * 使用lua脚本解锁，不会解除别人锁
     *
     * @param redisLockEntity
     * @return
     */
    public boolean unlockLua(RedisLock redisLockEntity) {
        if (redisLockEntity == null || redisLockEntity.getLockKey() == null || redisLockEntity.getRequestId() == null) {
            return false;
        }
        DefaultRedisScript<Long> redisScript = new DefaultRedisScript();
        //用于解锁的lua脚本位置
        redisScript.setLocation(new ClassPathResource("unlock.lua"));
        redisScript.setResultType(Long.class);
        //没有指定序列化方式，默认使用上面配置的
        Object result = redisTemplate.execute(redisScript, Arrays.asList(redisLockEntity.getLockKey()), redisLockEntity.getRequestId());
        return result.equals(Long.valueOf(1));
    }
}
