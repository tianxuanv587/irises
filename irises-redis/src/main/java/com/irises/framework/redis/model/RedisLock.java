package com.irises.framework.redis.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class RedisLock {
    private String lockKey;
    private String requestId;
}