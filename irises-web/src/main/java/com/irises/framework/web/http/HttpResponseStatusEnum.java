package com.irises.framework.web.http;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum HttpResponseStatusEnum implements CommonResponse {

    SUCCESS(200, "success"),                            // 成功请求
    FORBIDDEN_OPERATION(403, "权限不足"),               // 权限不足
    FAIL(500, "error")               // 失败
    ;
    private int code;
    private String message;

    @Override
    public int getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public Object getResult() {
        return null;
    }

}
