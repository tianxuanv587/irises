package com.irises.framework.web.http;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * HTTP 统一响应结果
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class WebResponse {

    /**
     * 响应码
     */
    private int code;

    /**
     * 提示信息
     */
    private String message;

    /**
     * 结果
     */
    private Object data;

    public WebResponse(HttpResponseStatusEnum httpResponseStatusEnum) {
        this.code = httpResponseStatusEnum.getCode();
        this.message = httpResponseStatusEnum.getMessage();
    }

    public WebResponse(CommonResponse commonResponse) {
        this.code = commonResponse.getCode();
        this.message = commonResponse.getMessage();
    }

    public static WebResponse of(CommonResponse commonResponse){
        return new WebResponse(commonResponse);
    }

    /**
     * 成功响应
     */
    public static WebResponse success() {
        return new WebResponse(HttpResponseStatusEnum.SUCCESS.getCode(), HttpResponseStatusEnum.SUCCESS.getMessage(), null);
    }

    /**
     * 成功响应
     */
    public static WebResponse success(Object result) {
        return new WebResponse(HttpResponseStatusEnum.SUCCESS.getCode(), HttpResponseStatusEnum.SUCCESS.getMessage(), result);
    }

    /**
     * 禁止操作
     */
    public static WebResponse forbidden() {
        return new WebResponse(HttpResponseStatusEnum.FORBIDDEN_OPERATION.getCode(), HttpResponseStatusEnum.FORBIDDEN_OPERATION.getMessage(), null);
    }

    /**
     * 失败响应
     */
    public static WebResponse fail() {
        return new WebResponse(HttpResponseStatusEnum.FAIL.getCode(), HttpResponseStatusEnum.FAIL.getMessage(), null);
    }

    /**
     * 失败响应
     */
    public static WebResponse fail(Object result) {
        return new WebResponse(HttpResponseStatusEnum.FAIL.getCode(), HttpResponseStatusEnum.FAIL.getMessage(), result);
    }

}
